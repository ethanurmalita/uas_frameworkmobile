<!doctype html>
<template>
  <div class="home">
    <img alt="Vue logo" src="../assets/logo.png">
    <HelloWorld msg="Welcome to Your Vue.js App"/>
  </div>
</template>
<html>
	<head>
		<title>project uas</title>
		<script src="vue.js"></script>
		<script src='axios-master/dist/axios.min.js'></script>

		<style>
			input[type=text]{
				width: 100%;
			}
		</style>
	</head>
	<body>
	
		<div id='myapp'>
			
			<table border='1' width='80%' style='border-collapse: collapse;'>
				<tr>
					<th>Nama Penyanyi</th>
					<th>Judul Lagu</th>
					<th>Pencipta</th>
					<th></th>
				</tr>

				<!-- Add -->
				<tr>
					<td><input type='text' v-model='nama_penyanyi'></td>
					<td><input type='text' v-model='judul'></td>
					<td><input type='text' v-model='pencipta'></td>
					<td><input type='button' value='Add' @click='addRecord();'></td>
				</tr>

				<!-- Update/Delete -->
				<tr v-for='(user,index) in users'>
					<td><input type='text' v-model='user.nama_penyanyi' ></td>
					<td><input type='text' v-model='user.judul' ></td>
					<td><input type='text' v-model='user.pencipta' ></td>
					<td><input type='button' value='Update' @click='updateRecord(index,user.id);'>&nbsp;<input type='button' value='Delete' @click='deleteRecord(index,user.id)'></td>
				</tr>
			</table>
			
		</div>

		<!-- Script -->
		<script>
			var app = new Vue({
				el: '#myapp',
				data: {
					users: "",
					userid: 0,
					nama_penyanyi: "",
					judul: "",
					pencipta: ""
				},
				methods: {
					allRecords: function(){
						axios.post('record.php', {
						    request: 1
						})
						.then(function (response) {
							app.users = response.data;
						})
						.catch(function (error) {
						    console.log(error);
						});
						
					},
					addRecord: function(){

						if(this.nama_penyanyi != '' && this.judul != '' && this.pencipta != ''){
							axios.post('record.php', {
							    request: 2,
							    nama_penyanyi: this.nama_penyanyi,
							    judul: this.judul,
							    pencipta: this.pencipta
							})
							.then(function (response) {

								// Fetch records
							    app.allRecords();

							    // Empty values
							    app.nama_penyanyi = '';
							    app.judul = '';
							    app.pencipta = '';

							    alert(response.data);
							})
							.catch(function (error) {
							    console.log(error);
							});
						}else{
							alert('Fill all fields.');
						}
									
					},
					updateRecord: function(index,id){

						// Read value from Textbox
						var judul = this.users[index].judul;
						var pencipta= this.users[index].pencipta;

						if(judul !='' && pencipta !=''){
							axios.post('record.php', {
							    request: 3,
							    id: id,
							    judul: judul,
							    pencipta: pencipta
							})
							.then(function (response) {
								alert(response.data);
							})
							.catch(function (error) {
							    console.log(error);
							});
						}
					},
					deleteRecord: function(index,id){
						
						axios.post('record.php', {
						    request: 4,
						    id: id
						})
						.then(function (response) {

							// Remove index from users
						    app.users.splice(index, 1);
						    alert(response.data);
						})
						.catch(function (error) {
						    console.log(error);
						});
						  
					}	
				},
				created: function(){
					this.allRecords();
				}
			})

		</script>
	</body>
</html>